﻿using DMS.Application;
using DMS.Geometry;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Drawing;
using System.Collections.Generic;
using DMS.OpenGL;

namespace Example
{
	class Controller
	{
        private Texture background;
        private Texture obstacle;

        private bool initialized = false;

        private Ball ball1;
        private Ball helpBall;

        private Box2D player;
        private List<Box2D> hitable;
        private Box2D doubleMe;

        private float countHitablei = 3f;
        private float countHitablej = 20f;
       

        private void init()
        {
            background = TextureLoader.FromBitmap(Resourcen.springBranch);
            obstacle = TextureLoader.FromBitmap(Resourcen.fractal);

            ball1 = new Ball(0, 0, 0.05f, 0.05f);
            ball1.dir.Y = -0.03f;
            player = new Box2D(0.0f, -0.95f, 0.4f, 0.02f);
            hitable = new List<Box2D>();
            doubleMe = new Box2D(0.2f, 0f, 0.3f, 0.3f);

            for (int i = 0; i < countHitablei; i++)
            {
                for (int j = 0; j < countHitablej; j++)
                {
                    float breitebox = (2 / countHitablej);
                    Box2D box = new Box2D(-1 + breitebox * j, 0.5f - breitebox * 0.5f + i * breitebox, breitebox, breitebox);
                    hitable.Add(box);
                }
            }
            initialized = true;
    }

		private void Update(float updatePeriod)
		{
            if (initialized == false)
            {
                init();
            }

            if (Keyboard.GetState()[Key.Left])
			{
				player.X -= updatePeriod*1.2f;
			}
			else if (Keyboard.GetState()[Key.Right])
			{
				player.X += updatePeriod*1.2f;
			}

            if (ball1.box.Intersects(player))
            {
                float differ = (player.CenterX - ball1.box.CenterX);
                float playerhalf = player.SizeX * 0.5f;
                float anglestrength = 0.02f;

                float ratio = differ / playerhalf;
                float dx = ratio * anglestrength * (-1);
                float dy = ball1.dir.Y * (-1);

                ball1.dir.X = dx;
                ball1.dir.Y = dy;
                ball1.canCollide = true;
            }
            if (helpBall != null) // falls Helpball existiert
            {
                if (helpBall.box.Intersects(player))
                {
                    helpBall.canCollide = false;
                    float differ = (player.CenterX - helpBall.box.CenterX);
                    float playerhalf = player.SizeX * 0.5f;
                    float anglestrength = 0.02f;

                    float ratio = differ / playerhalf;
                    //float dx = 
                    //float dy = 

                    helpBall.dir.X = ratio * anglestrength * (-1); 
                    helpBall.dir.Y = helpBall.dir.Y * (-1);
                    helpBall.canCollide = true;
                }
            }
            

            if (ball1.box.Intersects(doubleMe) && (helpBall == null)) // wenn helpball nicht existiert
            {
                helpBall = new Ball(0, 0, 0.025f, 0.025f);
                helpBall.dir.Y = 0.03f;
            }

            for (int i = 0; i < hitable.Count; i++)
            {
                if (hitable[i].Intersects(ball1.box) && (ball1.canCollide == true))
                {
                    ball1.dir.Y = ball1.dir.Y * (-1);
                    hitable.RemoveAt(i);
                    i--;
                    ball1.canCollide = false;
                }
            }

            if (helpBall != null) // wenn helpball existiert
            {
                for (int i = 0; i < hitable.Count; i++)
                {
                    if (hitable[i].Intersects(helpBall.box))
                    {
                        hitable.RemoveAt(i);
                        i--;
                        helpBall.dir.Y = helpBall.dir.Y * (-1);
                        helpBall.canCollide = false;
                    }
                }
            }

            if ((ball1.box.MaxY < -1) | (ball1.box.MaxY > 1))
                {
                    ball1.dir.Y = ball1.dir.Y * -1;
                    ball1.canCollide = true;
            }
            if ((ball1.box.MaxX < -1) | (ball1.box.MaxX > 1))
            {
                ball1.dir.X = ball1.dir.X * -1;
                ball1.canCollide = true;
            }

            if (helpBall != null)
            {
                if ((helpBall.box.MaxY > 1))
                {
                    helpBall.dir.Y = helpBall.dir.Y * -1;
                    helpBall.canCollide = true;
                }
                if ((helpBall.box.MaxX < -1) | (helpBall.box.MaxX > 1))
                {
                    helpBall.dir.X = helpBall.dir.X * -1;
                    helpBall.canCollide = true;
                }
                if (helpBall.box.MaxY < -1)
                {
                    helpBall = null;
                }

            }


            ball1.update();
            if (helpBall != null)
            {
                helpBall.update();
            }
        }

		private void Render()
		{
            
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Color3(Color.White);
            DrawTexturedRect(new Box2D(-1, -1, 2, 2), background);
            GL.Enable(EnableCap.Blend);
            GL.Disable(EnableCap.Blend);

            GL.Color3(Color.CornflowerBlue);
			DrawComplex(player);
			DrawComplex(ball1.box);

            if (helpBall != null)
            {
                DrawComplex(helpBall.box);
            }

            DrawComplex(doubleMe);

            for(int i = 0; i < hitable.Count; i++)
            {
                //DrawComplex(hitable[i]);
                DrawTexturedRect(hitable[i], obstacle);
            }

			GL.LineWidth(2.0f);
			GL.Color3(Color.YellowGreen);
			DrawBoxOutline(player);
			DrawBoxOutline(ball1.box);
            
		}

        private static void DrawTexturedRect(Box2D Rect, Texture tex)
        {
            tex.Activate();
            GL.Begin(PrimitiveType.Quads);
            //when using textures we have to set a texture coordinate for each vertex
            //by using the TexCoord command BEFORE the Vertex command
            GL.TexCoord2(0.0f, 0.0f); GL.Vertex2(Rect.X, Rect.Y);
            GL.TexCoord2(1.0f, 0.0f); GL.Vertex2(Rect.MaxX, Rect.Y);
            GL.TexCoord2(1.0f, 1.0f); GL.Vertex2(Rect.MaxX, Rect.MaxY);
            GL.TexCoord2(0.0f, 1.0f); GL.Vertex2(Rect.X, Rect.MaxY);
            GL.End();
            //the texture is disabled, so no other draw calls use this texture
            tex.Deactivate();
        }

        private void DrawBoxOutline(Box2D rect)
		{
			GL.Begin(PrimitiveType.LineLoop);
			GL.Vertex2(rect.X, rect.Y);
			GL.Vertex2(rect.MaxX, rect.Y);
			GL.Vertex2(rect.MaxX, rect.MaxY);
			GL.Vertex2(rect.X, rect.MaxY);
			GL.End();
		}

		private void DrawComplex(Box2D rect)
		{
			var xQuarter = rect.X + rect.SizeX * 0.25f;
			var x3Quarter = rect.X + rect.SizeX * 0.75f;
			var yThird = rect.Y + rect.SizeY * 0.33f;
			var y2Third = rect.Y + rect.SizeY * 0.66f;
			GL.Begin(PrimitiveType.Polygon);
			GL.Vertex2(rect.CenterX, rect.MaxY);
			GL.Vertex2(x3Quarter, y2Third);
			GL.Vertex2(rect.MaxX, rect.CenterY);
			GL.Vertex2(x3Quarter, yThird);
			GL.Vertex2(rect.MaxX, rect.Y);
			GL.Vertex2(rect.CenterX, yThird);
			GL.Vertex2(rect.X, rect.Y);
			GL.Vertex2(xQuarter, yThird);
			GL.Vertex2(rect.X, rect.CenterY);
			GL.Vertex2(xQuarter, y2Third);
			GL.End();
		}

		[STAThread]
		private static void Main()
		{
			var app = new ExampleApplication();
			var controller = new Controller();
			app.Render += controller.Render;
			app.Update += controller.Update;
			app.Run();
		}
	}
}