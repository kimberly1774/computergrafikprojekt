﻿using DMS.Geometry;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example
{
    class Ball
    {
        public Box2D box;
        public Vector2 pos;
        public Vector2 dir;
        public bool canCollide = true;

        public Ball(float X, float Y, float sizeX, float sizeY) {
            pos = new Vector2(X, Y);
            box = new Box2D(X, Y, sizeX, sizeY);
            dir = new Vector2(0.0f, 0f);
            
            // gleiche geschwindigkeit fuer den vektor: richtungsvektor normalisieren + geschwindigkeitsvektor = dir.normalize

        }

        public void update()
        {
            box.X += dir.X;
            box.Y += dir.Y;
        }
    }
}
